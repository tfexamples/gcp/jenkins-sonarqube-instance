
resource "google_dns_managed_zone" "domain-zone" {
  name        = "domain-zone"
  dns_name    = "${var.domain}."
  description = "${var.domain} Zone"
  labels = {
    environment = "testing"
  }
}

resource "google_dns_record_set" "jenkins-recordset" {
  name = "jenkins.${google_dns_managed_zone.domain-zone.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.domain-zone.name

  rrdatas = [google_compute_instance.jenkins_instance.network_interface[0].access_config[0].nat_ip]

  depends_on = [
    google_compute_instance.jenkins_instance,
  ]
}

resource "google_dns_record_set" "sonarqube-recordset" {
  name = "sonarqube.${google_dns_managed_zone.domain-zone.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.domain-zone.name

  rrdatas = [google_compute_instance.sonarqube_instance.network_interface[0].access_config[0].nat_ip]

  depends_on = [
    google_compute_instance.sonarqube_instance,
  ]
}
