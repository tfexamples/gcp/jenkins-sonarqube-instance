output "jenkins_ip" {
  value = [google_compute_instance.jenkins_instance.network_interface[0].access_config[0].nat_ip]
}

output "sonarqube_ip" {
  value = [google_compute_instance.sonarqube_instance.network_interface[0].access_config[0].nat_ip]
}
