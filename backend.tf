terraform {
  backend "gcs" {
    bucket      = "bucket-tf-state-testing"
    prefix      = "terraform/state"
    credentials = "~/.gcp/compute-service-account.json"
  }
}
