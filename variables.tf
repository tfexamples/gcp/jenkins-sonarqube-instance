variable "project_id" {
  type        = string
  description = "GCP project id"
}

variable "ssh_user" {
  type        = string
  description = "SSH user to log in the instances"
}

variable "domain" {
  type        = string
  description = "Domain name to configure in Cloud DNS"
}

variable "region" {
  type        = string
  description = "GCP Region"
  default     = "us-east1"
}

variable "zone" {
  type        = string
  description = "GCP zone"
  default     = "us-east1-b"
}

variable "gcp_credentials_path" {
  type        = string
  description = "credentials path"
  default     = "~/.gcp/compute-service-account.json"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "n1-standard-1"
}

